/*
 * iomacros.h
 *
 * Created: 3/5/2020 5:26:17 PM
 *  Author: UniX
 */ 

#ifndef IOMACROS_H_
#define IOMACROS_H_

	//macros:
	#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
	#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
	#define FLIPBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))
	#define GETBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))

	
	/*
		example for "definitions":
		
		#define BUTTON1_PORT PINB
		#define BUTTON1_PIN PINB4
		#define LED_PORT PORTC
		#define LED_PIN PORTC3
	*/
	//definitions:
	#define BUTTON1_PORT PINB
	#define BUTTON1_PIN PINB1
	#define BUTTON2_PORT PINB
	#define BUTTON2_PIN PINB2
	#define LED_PORT PORTB
	#define LED_PIN PORTB3
	
	
	/*
		example for "combo definitions":
		#define READ_BUTTON1 GETBIT(BUTTON1_PORT,BUTTON1_PIN)
		#define SET_LED SETBIT(LED_PORT,LED_PIN)
	*/
	//combo definitions:
	#define READ_BUTTON1 GETBIT(BUTTON1_PORT,BUTTON1_PIN)
	#define READ_BUTTON2 GETBIT(BUTTON2_PORT,BUTTON2_PIN)

#endif /* IOMACROS_H_ */
